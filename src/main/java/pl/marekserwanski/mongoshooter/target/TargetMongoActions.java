package pl.marekserwanski.mongoshooter.target;

import org.springframework.stereotype.Component;
import pl.marekserwanski.mongoshooter.domain.car.CarDto;
import pl.marekserwanski.mongoshooter.domain.car.CarService;
import pl.marekserwanski.mongoshooter.utils.MeasurableDto;

import java.util.List;
import java.util.UUID;

@Component
public class TargetMongoActions {

    private static final int DEFAULT_ACTIONS_COUNT = 100;
    private final CarService carService;

    TargetMongoActions(CarService carService) {
        this.carService = carService;
    }

    CarDto simpleGetRecordFromDB() {
        return carService.getFirstCar();
    }

    public List<MeasurableDto> getListFromDB(Integer actionsCount) {
        return carService.getCarsList(parseActionsCount(actionsCount));
    }

    void makeSimpleRecordUpdate() {
        carService.updateFirstCar();
    }

    CarDto makeSimpleCRUDactions() {
        return simpleCRUDactions();
    }

    void makeMultipleCRUDactions() {
        for (int i=0 ; i<DEFAULT_ACTIONS_COUNT ; i++) {
            simpleCRUDactions();
        }
    }

    //does not make business sense, let's check what is total time of all CRUD actions
    private CarDto simpleCRUDactions() {
        String dummyCar = UUID.randomUUID().toString();

        carService.addNewCar(dummyCar);                   //C
        CarDto result = carService.getFirstCar();         //R
        carService.updateFirstCar();                      //U
        carService.deleteCar(dummyCar);                   //D

        return result;
    }

    private int parseActionsCount(Integer actionsCount) {
        if (actionsCount == null || actionsCount<=0) {
            return DEFAULT_ACTIONS_COUNT;
        }
        return actionsCount;
    }
}
