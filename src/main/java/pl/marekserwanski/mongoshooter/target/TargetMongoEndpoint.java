package pl.marekserwanski.mongoshooter.target;

import org.springframework.web.bind.annotation.*;
import pl.marekserwanski.mongoshooter.domain.car.CarDto;
import pl.marekserwanski.mongoshooter.utils.*;

import java.util.List;

import static java.util.Arrays.asList;
import static pl.marekserwanski.mongoshooter.utils.Timer.start;
import static pl.marekserwanski.mongoshooter.utils.TimingsResponse.buildSimpleResponse;
import static pl.marekserwanski.mongoshooter.utils.TimingsWithDataResponse.buildResponseWithDtos;

@RestController
@RequestMapping("/db")
public class TargetMongoEndpoint {

    private final TargetMongoActions actions;

    TargetMongoEndpoint(TargetMongoActions targetMongoActions) {
        this.actions = targetMongoActions;
    }

    @GetMapping("simple/{authType}")
    public TimingsWithDataResponse simpleGetRecordFromDB(@RequestParam Long frontendStartTime) {
        Timer timer = start();
        CarDto car = actions.simpleGetRecordFromDB();
        return buildResponseWithDtos(frontendStartTime, timer.getProccessTimeInMS(), asList(car));
    }

    @PostMapping("simple/{authType}")
    public TimingsResponse makeSimpleRecordUpdate(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        actions.makeSimpleRecordUpdate();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }

    @PostMapping("list/{authType}")
    public TimingsWithDataResponse getListFromDB(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        List<MeasurableDto> cars = actions.getListFromDB(timingsRequest.getActionsCount());
        return buildResponseWithDtos(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS(), cars);
    }

    @PostMapping("crud/{authType}")
    public TimingsResponse makeSimpleCRUDactions(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        actions.makeSimpleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }

    @PostMapping("multiplecrud/{authType}")
    public TimingsResponse makeComplexCRUDactions(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        actions.makeMultipleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }


}
