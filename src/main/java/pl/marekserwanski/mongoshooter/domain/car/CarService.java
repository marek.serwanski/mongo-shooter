package pl.marekserwanski.mongoshooter.domain.car;

import pl.marekserwanski.mongoshooter.utils.MeasurableDto;

import java.util.List;

import static java.time.Instant.now;
import static java.util.Date.from;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Pageable.ofSize;
import static pl.marekserwanski.mongoshooter.domain.car.CarConfig.DEFAULT_CAR_BRAND;

public class CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public CarDto getFirstCar() {
        return carRepository.findByBrand(DEFAULT_CAR_BRAND).map(carMapper::map).orElseThrow(CarNotFoundException::new);
    }

    public List<MeasurableDto> getCarsList(int count) {
        return carRepository.findAllByBrand(ofSize(count)).stream().map(carMapper::map).collect(toList());
    }

    public void updateFirstCar() {
        carRepository.findByBrand(DEFAULT_CAR_BRAND).ifPresentOrElse(
            car -> {
                car.setLastUpdateTime(from(now()));
                carRepository.save(car);
            }, () -> {
                throw new CarNotFoundException();
            });
    }

    public void addNewCar(String brand) {
        carRepository.findByBrand(brand).ifPresent(car -> {throw new CarAlreadyExistException(car.getBrand());});
        carRepository.save(new Car(brand));
    }

    public void deleteCar(String brand) {
        carRepository.findByBrand(brand).ifPresent(carRepository::delete);
    }
}
