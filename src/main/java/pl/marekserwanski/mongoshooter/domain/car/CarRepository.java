package pl.marekserwanski.mongoshooter.domain.car;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

interface CarRepository extends MongoRepository<Car, Long> {

    Optional<Car> findByBrand(String brand);

    List<Car> findAllByBrand(Pageable size);
}
