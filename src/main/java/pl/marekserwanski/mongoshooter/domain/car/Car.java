package pl.marekserwanski.mongoshooter.domain.car;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;
import java.util.Date;

import static java.time.Instant.now;
import static java.util.Date.from;


@Document
class Car implements Serializable {

    @MongoId
    private ObjectId id;
    private final String brand;
    private Date creationTime;
    private Date lastUpdateTime;

    public Car(String brand) {
        this.brand = brand;
        this.creationTime = from(now());
    }

    public String getBrand() {
        return brand;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
}
