package pl.marekserwanski.mongoshooter.domain.car;

class CarAlreadyExistException extends RuntimeException {

    CarAlreadyExistException(String brand) {
        super ("Car already exists: " + brand);
    }
}
