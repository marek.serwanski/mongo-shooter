package pl.marekserwanski.mongoshooter.domain.car;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.slf4j.LoggerFactory.getLogger;

@Configuration
class CarConfig {

    private static final Logger log = getLogger(CarConfig.class);
    static final String DEFAULT_CAR_BRAND = "Syrena";

    @Bean
    CarMapper carMapper() {
        return new CarMapper();
    }

    @Bean
    CarService carService(CarRepository carRepository, CarMapper carMapper) {
        initTestCars(carRepository);
        return new CarService(carRepository, carMapper);
    }

    private void initTestCars(CarRepository carRepository) {
        carRepository.findByBrand(DEFAULT_CAR_BRAND).ifPresentOrElse(
                car -> log.info("default car '{}' already setup", car.getBrand()),
                () -> {
                    Car defaultCar = new Car(DEFAULT_CAR_BRAND);
                    carRepository.save(defaultCar);
                    log.info("created car user");
                });
    }
}
