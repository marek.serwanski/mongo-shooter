package pl.marekserwanski.mongoshooter.domain.car;

class CarMapper {

    CarDto map(Car car) {
        return new CarDto(car.getBrand());
    }
}
