package pl.marekserwanski.mongoshooter.domain.car;

class CarNotFoundException extends RuntimeException {

    CarNotFoundException() {
        super ("Car not found");
    }
}
