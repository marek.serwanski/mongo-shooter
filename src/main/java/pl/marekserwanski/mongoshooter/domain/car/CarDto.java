package pl.marekserwanski.mongoshooter.domain.car;

import pl.marekserwanski.mongoshooter.utils.MeasurableDto;

public class CarDto implements MeasurableDto {

    private final String brand;

    public CarDto(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }
}
