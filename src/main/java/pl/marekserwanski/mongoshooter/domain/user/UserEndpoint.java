package pl.marekserwanski.mongoshooter.domain.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@RestController
public class UserEndpoint {

    private final UserDaoAuthenticator userDaoAuthenticator;
    private final String AUTH_HEADER = "Authorization";

    @Autowired
    public UserEndpoint(UserDaoAuthenticator userDaoAuthenticator) {
        this.userDaoAuthenticator = userDaoAuthenticator;
    }


    @PostMapping("/logout")
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    //to make simple auth world easier to handle, we accept both get and post and create default user authentication
    @RequestMapping("/login")
    public void login(HttpSession session, HttpServletResponse response) {
        SecurityContext securityContext = userDaoAuthenticator.authenticateDefaultUser();
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, securityContext);

        //allow frontend to save tokens
        response.setHeader("Access-Control-Expose-Headers", AUTH_HEADER);
        response.setHeader(AUTH_HEADER, userDaoAuthenticator.createDefaultUserAuthHeader());
    }
}
