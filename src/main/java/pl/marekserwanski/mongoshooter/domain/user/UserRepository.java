package pl.marekserwanski.mongoshooter.domain.user;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

interface UserRepository extends MongoRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
