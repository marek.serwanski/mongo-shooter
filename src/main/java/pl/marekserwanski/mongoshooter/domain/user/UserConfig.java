package pl.marekserwanski.mongoshooter.domain.user;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

import static org.slf4j.LoggerFactory.getLogger;

@Configuration
public class UserConfig {

    private static final Logger log = getLogger(UserConfig.class);
    private static final String DEFAULT_USER = "user";

    @Bean
    public UserService userService(UserRepository userRepository) {
        setupTestUsers(userRepository);
        return new UserService(userRepository);
    }

    @Bean
    public UserDaoAuthenticator userDaoAuthenticator(DaoAuthenticationProvider authenticationProvider) {
        return new UserDaoAuthenticator(authenticationProvider);
    }

    private void setupTestUsers(UserRepository userRepository) {
        userRepository.findByUsername(DEFAULT_USER).ifPresentOrElse(
            user -> log.info("default user '{}' already setup", user.getUsername()),
            () -> {
                User defaultUser = new User();
                defaultUser.setUsername("user");
                defaultUser.setPassword("$2a$10$8.UnVuG9HHgffUDAlk8qfOuVGkqRzgVymGe07xd00DMxs.AQubh4a");
                userRepository.save(defaultUser);
                log.info("created default user");
            });
    }

}
