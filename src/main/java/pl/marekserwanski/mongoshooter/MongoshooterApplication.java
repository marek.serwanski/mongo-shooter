package pl.marekserwanski.mongoshooter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoshooterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoshooterApplication.class, args);
	}

}
