package pl.marekserwanski.mongoshooter.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.marekserwanski.mongoshooter.utils.Timer.start;

class TimerTest {

    @Test
    public void shouldCountMilis() {
        Timer timer = start();

        Long firstCheck = timer.getProccessTimeInMS();
        quickWait();
        Long secondCheck = timer.getProccessTimeInMS();

        assertThat(firstCheck).isNotNull();
        assertThat(secondCheck).isGreaterThan(firstCheck);
    }

    private void quickWait() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
