package pl.marekserwanski.mongoshooter.domain.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.mongoshooter.domain.user.UserDaoAuthenticator.*;

@ExtendWith(MockitoExtension.class)
public class UserDaoAuthenticatorTest {

    @Mock
    DaoAuthenticationProvider authenticationProvider;
    @InjectMocks
    UserDaoAuthenticator authenticator;

    @Test
    public void shouldAuthenticateUser() {
        Authentication authentication = new UsernamePasswordAuthenticationToken(DEFAULT_USERNAME, DEFAULT_PASS);
        when(authenticationProvider.authenticate(any(Authentication.class))).thenReturn(authentication);

        SecurityContext securityContext = authenticator.authenticateDefaultUser();

        Authentication result = securityContext.getAuthentication();
        assertThat(result.getPrincipal()).isEqualTo(DEFAULT_USERNAME);
        assertThat(result.getCredentials()).isEqualTo(DEFAULT_PASS);
    }
}
