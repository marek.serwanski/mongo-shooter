package pl.marekserwanski.mongoshooter.domain.user;

import static pl.marekserwanski.mongoshooter.domain.user.UserDaoAuthenticator.*;

class TestUsersFactory {

    static User defaultUser() {
        User user = new User();
        user.setUsername(DEFAULT_USERNAME);
        user.setPassword(DEFAULT_PASS);
        return user;
    }
}
